//needs jquery for the animations

"use strict";
var counter1 = 0;
const elem1 = document.getElementById("slidingVertical1");
var colors = ['#ffb81c', '#F9E167', '#67F9E1']

function startChangingText(textList, textDuration = 3) {
	// sets an interval that every "textDuration" seconds updates the text
	// by running change() with textList as a parameter
	// parameters: textList (a list of the elements you want to change)
	// textDuration: an int of the seconds that every element stays on screen
	textDuration *= 2000
	elem1.innerHTML = textList[0];
	const inst = setInterval(function () { change(textList); }, textDuration);
}

function change(textList) {
	// a function that fades out an element (document.getElementById("changing"))
	// and when this is done, it changes the text of it to be the next element of the
	// textList. When the textList is done, it restarts its counter, so it reuses the first
	// element.
	// this function is not supposed to be used seperately from startChangingText
	$("#slidingVertical1").fadeOut('slow');
	$("#slidingVertical1").promise().done(function () {
		elem1.innerHTML = textList[counter1];
		$("#slidingVertical1").css("color", colors[counter2]);
		$("#slidingVertical1").fadeIn('slow');
	});
	counter1++;
	if (counter1 >= textList.length) {
		counter1 = 0;
	}
}

 
// ##############################################################################################


var counter2 = 0;
const elem2 = document.getElementById("slidingVertical2");

function startChangingText1(textList, textDuration = 3) {
	// sets an interval that every "textDuration" seconds updates the text
	// by running change() with textList as a parameter
	// parameters: textList (a list of the elements you want to change)
	// textDuration: an int of the seconds that every element stays on screen
	textDuration *= 2000
	elem2.innerHTML = textList[0];
	const inst = setInterval(function () { change1(textList); }, textDuration);
}

function change1(textList) {
	// a function that fades out an element (document.getElementById("changing"))
	// and when this is done, it changes the text of it to be the next element of the
	// textList. When the textList is done, it restarts its counter, so it reuses the first
	// element.
	// this function is not supposed to be used seperately from startChangingText
	$("#slidingVertical2").fadeOut('slow');
	$("#slidingVertical2").promise().done(function () {
		elem2.innerHTML = textList[counter2];
		$("#slidingVertical2").css("color", colors[counter2]);
		$("#slidingVertical2").fadeIn('slow');
	});
	counter2++;
	if (counter2 >= textList.length) {
		counter2 = 0;
	}
}